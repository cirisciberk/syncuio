Automata ----------------------
letter a:       35,0    8,0     88,1    90,0    92,0    74,1    26,0    7,0     37,0     75,1    56,1    93,1    30,0    29,1    67,0    63,1    0,0     49,1    96,0     42,0    64,1    84,1    82,0    98,0    43,0    75,0    8,1     19,0    96,1     3,1     29,0    54,1    87,0    72,0    86,0    22,1    81,0    15,1    81,0     95,0    58,1    55,1    63,0    20,0    39,1    72,1    60,0    32,1    74,0     33,0
letter b:       89,0    73,1    21,1    61,0    5,1     73,0    66,1    36,1    46,0     52,0    57,0    91,0    12,1    17,0    78,1    18,1    62,1    86,1    97,0     95,1    70,1    67,0    35,1    24,1    10,1    97,1    58,1    84,0    38,0     94,1    4,1     61,1    53,1    54,1    32,1    65,1    10,0    3,0     49,0     24,0    88,1    75,1    65,0    39,1    23,1    66,0    90,1    64,1    86,1     60,1

UIO SEQUENCE FOR STATE 2
-----------------------
Greedy path: a a b a a b b
Greedy path length: 7
Greedy Time: 1219061 microseconds

SynchroP path: b b b b b
SynchroP path length: 5
SynchroP Time: 1025124 microsseconds

Automata ----------------------
letter a:       5,1     1,1     3,0     0,0     6,1     7,0     2,1     0,1
letter b:       3,1     1,0     7,1     4,1     5,1     6,0     2,1     1,1

UIO SEQUENCE FOR STATE 4
-----------------------
Greedy path: a b a a
Greedy path length: 4
Greedy Time: 85 microseconds

SynchroP path: b b b
SynchroP path length: 3
SynchroP Time: 82 microsseconds

