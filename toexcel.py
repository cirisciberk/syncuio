from openpyxl import load_workbook, Workbook

txt = open("asd.txt","r").readlines()
#print(txt)

automata_list = []
temp_list = []
inputs_list = []

states = "32" 
inputs = "2"
outputs = "2"
selected_state = "2"

wb = Workbook()
ws = wb.active

ws.cell(row=1, column=2).value = "Number of States"
ws.cell(row=1, column=3).value = "Number of Inputs"
ws.cell(row=1, column=4).value = "Number of Outputs"
ws.cell(row=1, column=5).value = "Selected State"
ws.cell(row=1, column=6).value = "Random Seed"

ws.cell(row=1, column=7).value = "Greedy path"
ws.cell(row=1, column=8).value = "Greedy path length"
ws.cell(row=1, column=9).value = "Greedy Time"

ws.cell(row=1, column=10).value = "SynchroP path"
ws.cell(row=1, column=11).value = "SynchroP path length"
ws.cell(row=1, column=12).value = "SynchroP Time"

count = 2
for n in range(0,200,2):
  ws.cell(row=count, column=2).value = states
  ws.cell(row=count, column=3).value = inputs
  ws.cell(row=count, column=4).value = outputs
  ws.cell(row=count, column=5).value = selected_state
  ws.cell(row=count, column=6).value = n
  
  
  count += 1
  
row = 2

for j in txt:
  
  if ("Greedy path:" in j):
    ws.cell(row=row, column=7).value = j[12:-1]
  
  elif ("Greedy path length:" in j):
    ws.cell(row=row, column=8).value = j[19:-1]
  
  elif ("Greedy Time:" in j):
    ws.cell(row=row, column=9).value = j[13:-1]
  
  elif ("SynchroP path:" in j):
    ws.cell(row=row, column=10).value = j[15:-1]
  
  elif ("SynchroP path length:" in j):
    ws.cell(row=row, column=11).value = j[21:-1]
  
  elif ("SynchroP Time:" in j):
    ws.cell(row=row, column=12).value = j[15:-1]
    row += 1
    
    
for col in ws.columns:  #this block makes columns fitted according to the text
    max_length = 0
    column = col[0].column  # Get the column name
    for cell in col:
        if cell.coordinate in ws.merged_cells:  # not check merge_cells
            continue
        try:  # Necessary to avoid error on empty cells
            if len(str(cell.value)) > max_length:
                max_length = len(cell.value)
        except:
            pass

    adjusted_width = (max_length)
    ws.column_dimensions[column].width = adjusted_width + 2
    
wb.save(states + " " + inputs + " " + outputs + " " + selected_state + " " +  ".xlsx")
wb.close()

