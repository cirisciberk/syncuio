CC=g++
CFLAGS=-O3 -std=c++11 -g
FILES= syncUIO.cpp

syncUIO: $(FILES)
	$(CC) $(FILES) $(CFLAGS) -o syncUIO;
	@echo "syncUIO is compiled" 
clean:
	rm -rf syncUIO