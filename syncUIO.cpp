#include <iostream>
#include <fstream>
#include <stdint.h>
#include <math.h>
#include <cstring>
#include <chrono>
#include "limits.h"
#include <algorithm>
#include <tuple>
#include <memory>

#define Id(s1, s2) ((s1 > s2)?(((s1 * (s1 + 1))/2) + s2):(((s2 * (s2 + 1))/2) + s1)) //this is how we compute the ids
#define s1fromId(id) ((int)(sqrt((2.0 * id) +1.0) - 0.5));
#define s2fromId(id, s1) (id - ((s1 * (s1 + 1))/2));
#define PRINTAUTOMATA
//#define PRINTPAIR
//#define DEBUGPAIR
#define PRINTPATH
//#define CHECKHS

using std::cout;
using std::endl;

struct PNode {
  int letter;
  PNode* next;
  PNode(int _letter, PNode* _next) : letter(_letter), next(_next) {}
};

typedef std::chrono::steady_clock::time_point Time; 

uint64_t getBaseID(unsigned short * list, int size, unsigned short base)
{
  int result = 0;
  for(int i = 0; i < size; i++)
  {
    result += list[i]*pow(base,i);
  }
  return result;
}

//This function is converting a decimal number to base-decimal list.
int* fromBaseID(int64_t num, int size, int base)
{
  int* path = new int[size];
  for(int i = 0; i < size; i++)
  {
    path[size-i-1] = num % base;
    num /= base;
  }

  return path;
}

unsigned short getUniquePairID(unsigned short s1, unsigned short s2, int N)
{
  unsigned short result = 0;
  for(int i = 0; i < s1; i++)
  {
    result += (N-i-1);
  }

  result += (s2 - s1 -1);
  return result;
}

unsigned short* fromUniquePairID(unsigned short num, int N)
{
  unsigned short* pair = new unsigned short[2];
  if(num == N*(N-1)/2)
  {
    pair[0] = N;
    pair[1] = N;
    return pair;
  } 

  unsigned short compare = 0;
  unsigned short j = -1;
  unsigned short k;
  while(compare <= num)
  {
    j++;
    compare += (N-j-1);
  }

  compare -= (N-j-1);
  k = num - compare + j + 1;
  pair[0] = j;
  pair[1] = k;
  return pair;
}

void printFSM(unsigned short ** nextStates, unsigned short ** outputs, int N, int p) {
  cout << "Automata ----------------------" << endl;
  for (int i = 0; i < p; ++i) {
    cout << "letter " << (char)(i + 97) << ":\t";
    for (int j = 0; j < N; ++j) {
      cout << nextStates[i][j] << "," << outputs[i][j] << "\t";
    }
    cout << endl;
  }
}

void printPairAutomata(unsigned short ** pairAutomata, int N, int p) {
  cout << "Pair Automata ----------------------" << endl;
  for (int i = 0; i < p; i++) {
    cout << "letter " << (char)(i + 97) << ":\t";
    for (int j = 0; j < N; j++) {
      for(int k = j + 1; k < N; k++)
      {
        unsigned short * pairNS = fromUniquePairID(pairAutomata[i][getUniquePairID(j,k,N)], N);
        cout << j << "," << k << "/" << pairNS[0] << "," << pairNS[1] << "\t\t";
      }
      cout << endl;
    }
  }
}

unsigned short ** convertToPairAutomata(unsigned short N, unsigned short P, int noOfPair, unsigned short ** nextStates, unsigned short ** outputs)
{
  unsigned short ** pairAutomata = new unsigned short*[P];

  for (int i = 0; i < P; i++)
  {
    pairAutomata[i] = new unsigned short[noOfPair + 1];
  }

  for(int i = 0; i < P; i++)
  {
    pairAutomata[i][noOfPair] = noOfPair;
    for(int j = 0; j < N; j++)
    {
      for(int k = j+1; k < N; k++)
      {
        unsigned short id = getUniquePairID(j,k, N);

        if(nextStates[i][j] == nextStates[i][k] || outputs[i][j] != outputs[i][k])
        {
          pairAutomata[i][id] = noOfPair; 
        }
        else
        {
          unsigned short nS1 = nextStates[i][j];
          unsigned short nS2 = nextStates[i][k];
          if(nS1 > nS2)
          {
            unsigned short temp = nS1;
            nS1 = nS2;
            nS2 = temp;
          }

          pairAutomata[i][id] = getUniquePairID(nS1,nS2, N);
        }

#ifdef DEBUGPAIR
          cout << j << "," << k << "'s id is " << id << " and results id is " << pairAutomata[i][id] << endl;
#endif
      }
    }
  }
  return pairAutomata;
}

void insertToPath(int letter, PNode* &head, PNode* &last) {
  PNode* temp = new PNode(letter, NULL);
  if (head == NULL) {
    head = last = temp;
  } else {
    last = last->next = temp;
  }
}
char isHomingSequence(unsigned short ** nextStates, unsigned short ** outputs, unsigned short N, unsigned short O, int * path, unsigned short length)
{
  //tempNextStates will store last states at the end
  //outputResults will store output sequences from each state according to input sequence
  unsigned short ** outputResults = new unsigned short*[N];
  unsigned short * tempNextStates = new unsigned short[N];

  //tempNextStates should start with all states as they are all active  
  for (int i = 0; i < N; i++)
  {
    outputResults[i] = new unsigned short[length];
    tempNextStates[i] = i;
  }

  //Filling the created elemenst above according to automata and input sequence
  for (int j = 0; j < length; j++)
  {
    for (int i = 0; i < N; i++)
    {
      outputResults[i][j] = outputs[path[j]][tempNextStates[i]];
      tempNextStates[i] = nextStates[path[j]][tempNextStates[i]];       
    }
  }

  //Checking the result if it is a homing sequence
  for(int i = 0; i < N; i++)
  {
    for(int j = i + 1; j < N; j++)
    {
      //To compare outputs, first we are acting like it is a length-decimal number and comparing it to decimal
    int a = getBaseID(outputResults[i],length,O);
    int b = getBaseID(outputResults[j],length,O);

    //cout << a << " " << b << endl;
      if(a == b)
      {
        //if we can get same output from different final states, then gg.
        if(tempNextStates[i] != tempNextStates[j])
        {
          for(int k = 0; k < N; k++)
          {
            delete outputResults[k];
          }
          delete outputResults;
          delete tempNextStates;
          return 'F';
        }
      }
    }
  }
  for(int k = 0; k < N; k++)
  {
    delete outputResults[k];
  }
  delete outputResults;
  delete tempNextStates;
  return 'T';
}

unsigned short shortestHomingSequence(unsigned short ** nextStates, unsigned short ** outputs, unsigned short N, unsigned short P, unsigned short O)
{
  int end = P;
  unsigned short size = 1;
  int limit = N*(N-1)/2 + 1;
  while(true)
  {

    for(int j = 0; j < end; j++)
    {
      //Creating path with converting j to size-decimal number.
      int * path = new int[size];
      path = fromBaseID(j, size, P);

      //if we found a result, this function can return
      if(isHomingSequence(nextStates, outputs, N, O, path, size)=='T')
      {
        delete path;
        return size;
      }
    }

    end *= P;
    size++;
    //This part is unnecessary but we know that length of a hs cant be greater than n*(n-1)/2
    if(size == limit)
    {
      return size;
    }
  }
}

// implementation of delta output function extended for input sequences
int delta(std::vector<int> seq, int state, unsigned short** nextStates){
  int t_state = state;
  for(auto i=seq.begin();i!=seq.end();i++)
    t_state = nextStates[*i][t_state]; 
  return t_state;
}

// sort the outputs to group them
void sort_outputs(std::vector<std::vector<int>> &vect, int* &row){

std::vector<std::pair<std::string, int>> newVect;
int rrr=vect.size();
newVect.reserve(rrr);
int* r=new int[rrr+1];
int* rr=new int[rrr+1];
for(int i=0;i<=rrr;i++){
  r[i]=rr[i]=row[i];
}

int index = 0;
for(const std::vector<int>& each_row: vect)
  {
    std::string str;             // each row of vect to a single integer string
    for(const int Integer: each_row) str += std::to_string(Integer);
    newVect.emplace_back(std::make_pair(str, index));
    ++index;
  }
  // sort the new vector, according to the whole 2D vector row(which is a single string)
  std::sort(newVect.begin(), newVect.end(), [](decltype(*newVect.cbegin()) lhs,decltype(lhs) rhs)
  {
    return lhs.first < rhs.first;
  });
  // now you can actually store the sorted vect
  vect.clear();
  for(auto index = 1; index <= row[0]; ++index)
  {
    row[index] = newVect[index-1].second;  // replace the row indexes which are sorted
    std::vector<int> vect_row;
    // change each chars of string back to corresponding row elements
    for(const char Integer: newVect[index-1].first)
      vect_row.emplace_back(static_cast<int>(Integer - '0'));
    // store to original vector
    vect.emplace_back(vect_row);
  }
  // new array contains indexes so take the real values from indexes
  for(int i=0;i<rrr;i++)
    r[i+1]=rr[row[i+1]+1];

  for(int i=0;i<=rrr;i++)
    row[i]=r[i];

}

void applyseq_fast(PNode* &last,PNode* &path,int** &matrix,int &rows,unsigned short** a,int* distance,int* letter,int* next,unsigned short** outputs){
       
      std::vector<std::vector<int>> temp_matrix;
      int position=0;
      int pair=-1;
      for(int i=0;i<rows;i++){
        if(matrix[i][0]==0) continue;
        pair=Id(matrix[i][1],matrix[i][2]); // take first pair
        break;
      }
      std::vector<int> seq;
      while(pair!=-1 && distance[pair]!=0){ // add this homing word to sequence
        seq.push_back(letter[pair]);
        insertToPath(letter[pair], path, last);
        pair=next[pair];     
      }
      
      for(int i=0;i<rows;i++){
        position++;
        if(matrix[i][0]==0) continue;
          std::vector<std::vector<int>> Outputs; 
          Outputs.resize(matrix[i][0]);
          for(int e=0;e<matrix[i][0];e++){
            int state=matrix[i][e+1];
            for(int elm=0;elm<seq.size();elm++){
              Outputs[e].push_back(outputs[seq[elm]][state]); // for all states keep their outputs to the sequence selected
              state=a[seq[elm]][state];
            }
          }
          sort_outputs(Outputs,matrix[i]); // sort outputs to group them; sort the new states according to their outputs
          int counter=1; 
          std::vector<int> t;
          t.push_back(1);
          t.push_back(delta(seq,matrix[i][counter++],a)); 
          temp_matrix.push_back(t);
          
          for(int r=1;r<Outputs.size();r++){ // can easily process since outputs & states are sorted            
                if(Outputs[r]==Outputs[r-1]){ // if output is equal to the previous one's output => they're not homed
                  int next_state=delta(seq,matrix[i][counter++],a);
                    temp_matrix[position-1].push_back(next_state);
                    ++temp_matrix[position-1][0];
                  }
                else{ // if output is not equal to the previous one's output => they re homed; add that state to the next row
                  position++;
                  std::vector<int> te;
                  te.push_back(1);
                  te.push_back(delta(seq,matrix[i][counter++],a)); 
                  temp_matrix.push_back(te);  
                }
          }
      }
      rows=position;
      for(int i=0;i<position;i++){
        if(temp_matrix[i].size()>2){
          std::sort(temp_matrix[i].begin()+1,temp_matrix[i].end());
          temp_matrix[i].erase(unique(temp_matrix[i].begin()+1,temp_matrix[i].end()),temp_matrix[i].end()); // keep the unique ones
          temp_matrix[i][0]=temp_matrix[i].size()-1;
        }
        for(int j=0;j<=temp_matrix[i][0];j++){
          matrix[i][j]=temp_matrix[i][j];
        }        
      }
    }

/////////////////////////////// ORIGINAL APPLY SEQUENCE FUNCTION FOR GREEDY HS ////////////////////////////////////////////////
void applyseq_greedy(PNode* &last,PNode* &path,int** &matrix,int &rows,unsigned short** a,int* distance,int* letter,int* next,unsigned short** outputs){
       
      std::vector<std::vector<int>> temp_matrix;
      int position=0;
      int pair=-1;
      int min_distance=INT_MAX;
      for(int i=0;i<rows;i++){
        if(matrix[i][0]==0) continue;
        for(int j=1;j<=matrix[i][0];j++){ // take the pair that has a lowest homing word length in the created BFS forest 
          for(int jj=1;jj<j;jj++){
            if(distance[Id(matrix[i][j],matrix[i][jj])]<min_distance){
              pair=Id(matrix[i][j],matrix[i][jj]);
              min_distance=distance[pair];
              if(min_distance==1) goto DONE;
            }
          }
        }
      }

      DONE:

      std::vector<int> seq;
      while(pair!=-1 && distance[pair]!=0){ // add this homing word to sequence
        seq.push_back(letter[pair]);
        insertToPath(letter[pair], path, last);
        pair=next[pair];     
      }
      
      for(int i=0;i<rows;i++){
        position++;
        if(matrix[i][0]==0) continue;
          std::vector<std::vector<int>> Outputs;
          Outputs.resize(matrix[i][0]);
          for(int e=0;e<matrix[i][0];e++){
            int state=matrix[i][e+1];
            for(int elm=0;elm<seq.size();elm++){

              Outputs[e].push_back(outputs[seq[elm]][state]); // for all states keep their outputs to the sequence selected
              state=a[seq[elm]][state];
            }
          }
          sort_outputs(Outputs,matrix[i]); // sort output to group them; sort the new states according to their outputs
          int counter=1; 
          std::vector<int> t;
          t.push_back(1);
          t.push_back(delta(seq,matrix[i][counter++],a)); 
          temp_matrix.push_back(t);
          
          for(int r=1;r<Outputs.size();r++){ // can easily process since outputs & states are sorted             
                if(Outputs[r]==Outputs[r-1]){ // if output is equal to the previous one's output => they're not homed
                  int next_state=delta(seq,matrix[i][counter++],a);
                    temp_matrix[position-1].push_back(next_state);
                    ++temp_matrix[position-1][0];
                  }
                else{ // if output is not equal to the previous one's output => they re homed; add that state to the next row
                  position++;
                  std::vector<int> te;
                  te.push_back(1);
                  te.push_back(delta(seq,matrix[i][counter++],a)); 
                  temp_matrix.push_back(te);  
                }
          }
      }
      rows=position;
      for(int i=0;i<position;i++){
        if(temp_matrix[i].size()>2){
          std::sort(temp_matrix[i].begin()+1,temp_matrix[i].end());
          temp_matrix[i].erase(unique(temp_matrix[i].begin()+1,temp_matrix[i].end()),temp_matrix[i].end()); // keep the unique ones
          temp_matrix[i][0]=temp_matrix[i].size()-1;
        }
        for(int j=0;j<=temp_matrix[i][0];j++){
          matrix[i][j]=temp_matrix[i][j];
        }        
      }
    }

    /*
    bool isPrefixOf(std::vector<int> current, std::vector<int> other) 
    {
      if (other.size() > current.size())
        {
          return false;
        }
      for (int i = 0; i < other.size(); ++i) 
        {
          if (other[i] != current[i]) 
            {
              return false; // clearly not a prefix
            }
        }

      return true; // we match up, so we must be done!
        
    }
    */

    struct Node
    {
      std::shared_ptr<Node> right=NULL;
      std::shared_ptr<Node> left=NULL;
      std::shared_ptr<Node> parent=NULL;
      int count=0;
      int id=-1;
    };


/////////////////////////////////////// JUST A TRIAL //////////////////////////////////////////////////////////////////////
    void applyseq_greedy_max(std::vector<std::shared_ptr<Node>> &ptrs,std::shared_ptr<Node> &root,PNode* &last,PNode* &path,int** &matrix,int &rows,unsigned short** a,int* distance,int* letter,int* next,unsigned short** outputs){
      int pair=-1;
      std::vector<std::vector<int>> temp_matrix;
      int position=0;

      std::vector<std::shared_ptr<Node>> leaves;
      for(int i=0;i<rows;i++){
        if(matrix[i][0]==0) continue;
        for(int j=1;j<=matrix[i][0];j++){
          for(int jj=1;jj<j;jj++){
              int ID=Id(matrix[i][j],matrix[i][jj]);
              (ptrs[ID])->count=1;
              std::shared_ptr<Node> temp=ptrs[ID];
              leaves.push_back(ptrs[ID]);
          }
        }
      }
        int temp_max=0;
        for(auto yusa=leaves.begin();yusa!=leaves.end();yusa++){
          
          int tot=0;
          std::shared_ptr<Node> traverse=*yusa;
          int pid=traverse->id;
          while(traverse->parent!=NULL){
            tot+=traverse->count;
            traverse=traverse->parent;
          }
          if(tot>=temp_max){
            temp_max=tot;
            pair=pid;
          }
        }
        for(auto yusa=leaves.begin();yusa!=leaves.end();yusa++){
          (*yusa)->count=0;
        }


/*
      int max=0;
      int len=actives1.size();
      int prefix_count;
      for(int l=0; l<len; l++){
        prefix_count=0;
        for(int ll=0; ll<len; ll++){
          if(isPrefixOf(actives0[l],actives0[ll])) ++prefix_count;
        }
        if(prefix_count>=max){
          max=prefix_count;
          pair=actives1[l];
          if(max==len) goto DONE;
        }
      }

      DONE:*/

      std::vector<int> seq;
      while(pair!=-1 && distance[pair]!=0){
        seq.push_back(letter[pair]);
        insertToPath(letter[pair], path, last);
        pair=next[pair];     
      }
      
      for(int i=0;i<rows;i++){
        position++;
        if(matrix[i][0]==0) continue;
          std::vector<std::vector<int>> Outputs;
          Outputs.resize(matrix[i][0]);
          for(int e=0;e<matrix[i][0];e++){
            int state=matrix[i][e+1];
            for(int elm=0;elm<seq.size();elm++){

              Outputs[e].push_back(outputs[seq[elm]][state]);
              state=a[seq[elm]][state];
            }
          }
          sort_outputs(Outputs,matrix[i]); 
          int counter=1; 
          std::vector<int> t;
          t.push_back(1);
          t.push_back(delta(seq,matrix[i][counter++],a)); 
          temp_matrix.push_back(t);
          
          for(int r=1;r<Outputs.size();r++){            
                if(Outputs[r]==Outputs[r-1]){
                  int next_state=delta(seq,matrix[i][counter++],a);
                    temp_matrix[position-1].push_back(next_state);
                    ++temp_matrix[position-1][0];
                  }
                else{
                  position++;
                  std::vector<int> te;
                  te.push_back(1);
                  te.push_back(delta(seq,matrix[i][counter++],a)); 
                  temp_matrix.push_back(te);  
                }
          }
      }
      rows=position;
      for(int i=0;i<position;i++){
        if(temp_matrix[i].size()>2){
          std::sort(temp_matrix[i].begin()+1,temp_matrix[i].end());
          temp_matrix[i].erase(unique(temp_matrix[i].begin()+1,temp_matrix[i].end()),temp_matrix[i].end());
          temp_matrix[i][0]=temp_matrix[i].size()-1;
        }
        for(int j=0;j<=temp_matrix[i][0];j++){
          matrix[i][j]=temp_matrix[i][j];
        }        
      }
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void applyseq_SynchroP(PNode* &last,PNode* &path,int** &matrix,int &rows,unsigned short** a,int* distance,int* letter,int* next,unsigned short** outputs){
       
      std::vector<std::vector<int>> temp_matrix;
      int position=0;
      int pair=-1;
      long min_distance=LLONG_MAX;
      for(int i=0;i<rows;i++){
        if(matrix[i][0]==0) continue;
        for(int j=1;j<=matrix[i][0];j++){
          for(int jj=1;jj<j;jj++){
            int dist=0;
            int pid=Id(matrix[i][j],matrix[i][jj]);
            int pid_backup=pid;
            std::vector<int> sequence;
            while(pid!=-1 && distance[pid]!=0){
              sequence.push_back(letter[pid]);
              pid=next[pid];     
            }
            std::vector<std::vector<int>> SynchroP_matrix;
            for(int rw=0;rw<rows;rw++){
              std::vector<int> temp_row;
              for(int colmn=1;colmn<=matrix[rw][0];colmn++){
                temp_row.push_back(delta(sequence,matrix[rw][colmn],a));
              }
            SynchroP_matrix.push_back(temp_row); 
            }

            for(int rw=0;rw<SynchroP_matrix.size();rw++){
              for(int colmn=0;colmn<SynchroP_matrix[rw].size();colmn++){
                for(int clm=0;clm<colmn;clm++){
                  dist+=distance[Id(SynchroP_matrix[rw][colmn],SynchroP_matrix[rw][clm])];
              }
            }
          }
            if(dist<min_distance){
              min_distance=dist;
              pair=pid_backup;
            }
          }
        }
      }

      std::vector<int> seq;
      while(pair!=-1 && distance[pair]!=0){
        seq.push_back(letter[pair]);
        insertToPath(letter[pair], path, last);
        pair=next[pair];     
      }
      
      for(int i=0;i<rows;i++){
        position++;
        if(matrix[i][0]==0) continue;
          std::vector<std::vector<int>> Outputs;
          Outputs.resize(matrix[i][0]);
          for(int e=0;e<matrix[i][0];e++){
            int state=matrix[i][e+1];
            for(int elm=0;elm<seq.size();elm++){

              Outputs[e].push_back(outputs[seq[elm]][state]);
              state=a[seq[elm]][state];
            }
          }
          sort_outputs(Outputs,matrix[i]); 
          int counter=1; 
          std::vector<int> t;
          t.push_back(1);
          t.push_back(delta(seq,matrix[i][counter++],a)); 
          temp_matrix.push_back(t);
          
          for(int r=1;r<Outputs.size();r++){            
                if(Outputs[r]==Outputs[r-1]){
                  int next_state=delta(seq,matrix[i][counter++],a);
                    temp_matrix[position-1].push_back(next_state);
                    ++temp_matrix[position-1][0];
                  }
                else{
                  position++;
                  std::vector<int> te;
                  te.push_back(1);
                  te.push_back(delta(seq,matrix[i][counter++],a)); 
                  temp_matrix.push_back(te);  
                }
          }
      }
      rows=position;
      for(int i=0;i<position;i++){
        if(temp_matrix[i].size()>2){
          std::sort(temp_matrix[i].begin()+1,temp_matrix[i].end());
          temp_matrix[i].erase(unique(temp_matrix[i].begin()+1,temp_matrix[i].end()),temp_matrix[i].end());
          temp_matrix[i][0]=temp_matrix[i].size()-1;
        }
        for(int j=0;j<=temp_matrix[i][0];j++){
          matrix[i][j]=temp_matrix[i][j];
        }        
      }
    }

bool is_subset(std::vector<std::vector<int>> A, std::vector<int> B){
  for(auto i=A.begin();i!=A.end();i++){
    if(std::includes((*i).begin(), (*i).end(), B.begin(), B.end())) return true;
  }
  return false;
} 

// simplify the matrix by deleting unnecessary rows
bool simplify(int** &matrix, int &rows){  
  std::vector<std::vector<int>> A;
  for(int i=0;i<rows;i++){
    std::vector<int> temp;
    for(int j=1;j<=matrix[i][0];j++){
      temp.push_back(matrix[i][j]); 
    }
    if(temp.size()==1 || is_subset(A,temp)==true){ // if the row is singleton or subset of another remove it.
      continue;
    }
    
    A.push_back(temp);
  }

  for(int k=0;k<A.size();k++){
    for(int l=1;l<=A[k].size();l++){
      matrix[k][l]=A[k][l-1];
    }
    matrix[k][0]=A[k].size();
  }

  rows=A.size();

  if(A.size()==0) return true;
  
  return false;
}


////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////// FAST HOMING HEURISTIC //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////


void fastHomingHeuristic(unsigned short ** a, unsigned short** outputs, int* iap, int* ia, int N, int P,int O, PNode* &path,Time &start1,Time &end1,Time &start2,Time &end2) {

  int noOfPair = (N * (N + 1)) / 2;
  int* distance = new int[noOfPair];
  int* next = new int[noOfPair];
  int* letter = new int[noOfPair];
  int* que = new int[noOfPair];
  int ord=0;

  for (int i = 0; i < noOfPair; i++) {
    distance[i] =  -1;
  }

  //BFS queue for the pairs
  int qs = 0;
  int qe = 0;

  start1=std::chrono::steady_clock::now();
  for (int i = 0; i < N; ++i) {
    int id = Id(i, i);
    distance[id] = 0;
    que[qe++] = id;
  }

  bool output_check = true;
  //there are more nodes in the queue
  while (qs < qe && qe < noOfPair) {
    int q_id = que[qs++];
    int q_dist = distance[q_id];

    //will process the pair with id q_id now
    int q_s1 = s1fromId(q_id); //the first state in the pair
    int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)

    int ia_id;
    if(output_check){
      for(int i=0; i<N; i++){
        for(int j=0; j<i; j++){
          for(int p=0; p<P; p++){
            if(outputs[p][i] != outputs[p][j]){
              ia_id = Id(i,j);
              if(distance[ia_id] < 0){
                distance[ia_id] = 1;
                next[ia_id] = -1; // -1 stands for the state q*
                letter[ia_id] = p;
                que[qe++] = ia_id;
              }
            }
          }
        }
      }
      output_check = false;
    }

    for (int p = 0; p < P; p++) {
      int* p_ia = &ia[p * N]; //this is the inverse automata for letter p
      int* p_iap = &iap[p * (N + 1)]; //and its state pointers
      int iap_s1_limit = p_iap[q_s1 + 1];
      int iap_s2_limit = p_iap[q_s2 + 1];

      if (p_iap[q_s2] == iap_s2_limit) continue;

      for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < iap_s1_limit; ++iap_s1_ptr) {
        int ia_s1 = p_ia[iap_s1_ptr];
        for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < iap_s2_limit; ++iap_s2_ptr) {
          int ia_s2 = p_ia[iap_s2_ptr];
          ia_id = Id(ia_s1, ia_s2);

          if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
            distance[ia_id] = q_dist + 1;
            next[ia_id] = q_id;
            letter[ia_id] = p;
            que[qe++] = ia_id;
          }
        }
      }
    }
  }

  end1=std::chrono::steady_clock::now();

  int* levels = new int[200];
  memset(levels, 0, 200 * sizeof(int));

  for (int i = 0; i < N; ++i) {
    for (int j = 0; j <= i; ++j) {
      int id = Id(i, j);

      if (distance[id] == -1) {
        for (int p = 0; p < P; p++) {
          unsigned short ts1 = a[p][i];
          unsigned short ts2 = a[p][j];
          int tid = Id(ts1, ts2);
          //cout << "tid " <<  tid << ": distance is " << distance[tid] << endl;
        }

        //cout << "automaton is not synchronizing. pair " << id << " - (" << i << ", " << j << ") is not mergable\n";
        exit(1);
      } else {
        levels[distance[id]]++;
      }
    }
  }

    int rows = 1;
    int cols = N+1;
    int** matrix = new int*[N];
    for (int i = 0; i < N; ++i){
      matrix[i] = new int[cols];
      matrix[i][0]=0;
    }

    for (int j = 1; j < cols; ++j)
      matrix[0][j]=j-1;

    matrix[0][0]=N;
    PNode* last=NULL;
    start2=std::chrono::steady_clock::now();
    while(1){
      applyseq_fast(last,path,matrix,rows,a,distance,letter,next,outputs);
      if(simplify(matrix,rows)==true) break;      
    }
    end2=std::chrono::steady_clock::now();


  delete distance;
  delete next;
  delete letter;
  delete que;

}


///////////////////////////////////////// GREEDY HS ///////////////////////////////////////////////////////////////////////

void greedyHomingHeuristic(unsigned short ** a, unsigned short** outputs, int* iap, int* ia, int N, int P,int O, PNode* &path,Time &start1,Time &end1,Time &start2,Time &end2) {

  int noOfPair = (N * (N + 1)) / 2;
  int* distance = new int[noOfPair];
  int* next = new int[noOfPair];
  int* letter = new int[noOfPair];
  int* que = new int[noOfPair];
  int ord=0;

  for (int i = 0; i < noOfPair; i++) {
    distance[i] =  -1;
  }

  //BFS queue for the pairs
  int qs = 0;
  int qe = 0;

  start1=std::chrono::steady_clock::now();
  for (int i = 0; i < N; ++i) {
    int id = Id(i, i);
    distance[id] = 0;
    que[qe++] = id;
  }

  bool output_check = true;
  //there are more nodes in the queue
  while (qs < qe && qe < noOfPair) {
    int q_id = que[qs++];
    int q_dist = distance[q_id];

    //will process the pair with id q_id now
    int q_s1 = s1fromId(q_id); //the first state in the pair
    int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)
    
    int ia_id;
    if(output_check){
      for(int i=0; i<N; i++){
        for(int j=0; j<i; j++){
          for(int p=0; p<P; p++){
            if(outputs[p][i] != outputs[p][j]){
              ia_id = Id(i,j);
              if(distance[ia_id] < 0){
                distance[ia_id] = 1;
                next[ia_id] = -1; // -1 stands for the state q*
                letter[ia_id] = p;
                que[qe++] = ia_id;
              }
            }
          }
        }
      }
      output_check = false;
    }

    for (int p = 0; p < P; p++) {
      int* p_ia = &ia[p * N]; //this is the inverse automata for letter p
      int* p_iap = &iap[p * (N + 1)]; //and its state pointers
      int iap_s1_limit = p_iap[q_s1 + 1];
      int iap_s2_limit = p_iap[q_s2 + 1];

      if (p_iap[q_s2] == iap_s2_limit) continue;

      for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < iap_s1_limit; ++iap_s1_ptr) {
        int ia_s1 = p_ia[iap_s1_ptr];
        for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < iap_s2_limit; ++iap_s2_ptr) {
          int ia_s2 = p_ia[iap_s2_ptr];
          ia_id = Id(ia_s1, ia_s2);

          if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
            distance[ia_id] = q_dist + 1;
            next[ia_id] = q_id;
            letter[ia_id] = p;
            que[qe++] = ia_id;
          }
        }
      }
    }
  }

  end1=std::chrono::steady_clock::now();

  int* levels = new int[200];
  memset(levels, 0, 200 * sizeof(int));

  for (int i = 0; i < N; ++i) {
    for (int j = 0; j <= i; ++j) {
      int id = Id(i, j);

      if (distance[id] == -1) {
        for (int p = 0; p < P; p++) {
          unsigned short ts1 = a[p][i];
          unsigned short ts2 = a[p][j];
          int tid = Id(ts1, ts2);
          //cout << "tid " <<  tid << ": distance is " << distance[tid] << endl;
        }

        //cout << "automaton is not synchronizing. pair " << id << " - (" << i << ", " << j << ") is not mergable\n";
        exit(1);
      } else {
        levels[distance[id]]++;
      }
    }
  }

int rows = 1;
    int cols = N+1;
    int** matrix = new int*[N];
    for (int i = 0; i < N; ++i){
      matrix[i] = new int[cols];
      matrix[i][0]=0;
    }

    for (int j = 1; j < cols; ++j)
      matrix[0][j]=j-1;

    matrix[0][0]=N;
    PNode* last=NULL;
    start2=std::chrono::steady_clock::now();
    while(1){
      applyseq_greedy(last,path,matrix,rows,a,distance,letter,next,outputs);
      if(simplify(matrix,rows)==true) break;      
    }
    end2=std::chrono::steady_clock::now();

    /*
  PNode* last = NULL;

  int is_homing = 0;
  int np = (N*(N-1))/2;
  int posit = 0;
  int home[np];
  for(int i=0; i<N; i++){
    for(int j=0; j<i; j++)
      home[posit++] = Id(i,j);
  }

  int np_pos = 0;
  int pid = 0;
  while (is_homing==0) {
    posit=0;
    int min_dist=N*N;
    for(int i=0;i<N;i++){
      for(int j=0;j<i;j++){
        if(home[posit]!=-1 && distance[home[posit]]>0 && distance[home[posit]]<min_dist){
          min_dist=distance[home[posit]];
          pid = home[posit];
        }
        ++posit;
      }
    }
    while(pid!=-1 && distance[pid]>0){ // loop until current pair merged or seperated
      posit = 0;
      int let = letter[pid];
      insertToPath(let, path, last);
      for(int i=0; i<N; i++){
        for(int j=0; j<i; j++){
          if(home[posit]!=-1){
            if(let==letter[home[posit]]){
              home[posit]=next[home[posit]];
              if(home[posit]!=-1){
              int s_1 = s1fromId(home[posit]);
              int s_2 = s2fromId(home[posit],s_1);
              if(s_1==s_2) home[posit]=-1;}
            }
            else{
              int s_1 = s1fromId(home[posit]);
              int s_2 = s2fromId(home[posit],s_1);
              if(outputs[let][s_1]!=outputs[let][s_2])
                home[posit] = -1;  
              else{
                if(a[let][s_1]==a[let][s_2])
                  home[posit] = -1;
                else
                  home[posit] = Id(a[let][s_1], a[let][s_2]);
              }
            }
          }
          ++posit;
        }
      }
      pid = next[pid];
    }

    posit=0;
    np_pos=0;
    for(int i=0;i<N;i++){
        for(int j=0;j<i;j++){
          if(home[posit]!=-1)
            ++np_pos;
          ++posit;
        }
      }

    is_homing = 1;
    if(np_pos!=0) is_homing = 0;

  
    
    PNode* pnode=path;
    PNode* backup=pnode;
    ord = 0;
    while (pnode) {
      cout << (char)(pnode->letter+97) << " "<<std::flush;
    pnode = pnode->next;
    ++ord;
    }
    
    int* sh_path= new int [ord];
    for(int j=0; j<ord; ++j){
      sh_path[j] = backup->letter;
      backup = backup->next;
    }
    cout<< "\n \n"<<isHomingSequence(a,outputs,N,O,sh_path,ord)<< "\n"<< std::flush;
    
    //}
  */  

  delete distance;
  delete next;
  delete letter;
  delete que;

}




void greedyHomingHeuristic_max(unsigned short ** a, unsigned short** outputs, int* iap, int* ia, int N, int P,int O, PNode* &path,Time &start,Time &end) {

  int noOfPair = (N * (N + 1)) / 2;
  int* distance = new int[noOfPair];
  int* next = new int[noOfPair];
  int* letter = new int[noOfPair];
  int* que = new int[noOfPair];
  int ord=0;

  for (int i = 0; i < noOfPair; i++) {
    distance[i] =  -1;
  }

  //BFS queue for the pairs
  int qs = 0;
  int qe = 0;

  for (int i = 0; i < N; ++i) {
    int id = Id(i, i);
    distance[id] = 0;
    que[qe++] = id;
  }

  bool output_check = true;
  //there are more nodes in the queue
  while (qs < qe && qe < noOfPair) {
    int q_id = que[qs++];
    int q_dist = distance[q_id];

    //will process the pair with id q_id now
    int q_s1 = s1fromId(q_id); //the first state in the pair
    int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)
    
    int ia_id;
    if(output_check){
      for(int i=0; i<N; i++){
        for(int j=0; j<i; j++){
          for(int p=0; p<P; p++){
            if(outputs[p][i] != outputs[p][j]){
              ia_id = Id(i,j);
              if(distance[ia_id] < 0){
                distance[ia_id] = 1;
                next[ia_id] = -1; // -1 stands for the state q*
                letter[ia_id] = p;
                que[qe++] = ia_id;
              }
            }
          }
        }
      }
      output_check = false;
    }

    for (int p = 0; p < P; p++) {
      int* p_ia = &ia[p * N]; //this is the inverse automata for letter p
      int* p_iap = &iap[p * (N + 1)]; //and its state pointers
      int iap_s1_limit = p_iap[q_s1 + 1];
      int iap_s2_limit = p_iap[q_s2 + 1];

      if (p_iap[q_s2] == iap_s2_limit) continue;

      for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < iap_s1_limit; ++iap_s1_ptr) {
        int ia_s1 = p_ia[iap_s1_ptr];
        for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < iap_s2_limit; ++iap_s2_ptr) {
          int ia_s2 = p_ia[iap_s2_ptr];
          ia_id = Id(ia_s1, ia_s2);

          if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
            distance[ia_id] = q_dist + 1;
            next[ia_id] = q_id;
            letter[ia_id] = p;
            que[qe++] = ia_id;
          }
        }
      }
    }
  }

  int* levels = new int[200];
  memset(levels, 0, 200 * sizeof(int));

  for (int i = 0; i < N; ++i) {
    for (int j = 0; j <= i; ++j) {
      int id = Id(i, j);

      if (distance[id] == -1) {
        for (int p = 0; p < P; p++) {
          unsigned short ts1 = a[p][i];
          unsigned short ts2 = a[p][j];
          int tid = Id(ts1, ts2);
          //cout << "tid " <<  tid << ": distance is " << distance[tid] << endl;
        }

        //cout << "automaton is not synchronizing. pair " << id << " - (" << i << ", " << j << ") is not mergable\n";
        exit(1);
      } else {
        levels[distance[id]]++;
      }
    }
  }

  std::vector<std::shared_ptr<Node>> ptrs;
  ptrs.resize(Id(N,N));
  std::shared_ptr<Node> root= std::make_shared<Node>();

  for(int i=0; i<N; ++i){
    for(int j=0; j<i; ++j){
      std::shared_ptr<Node> cur=root;
      int ID=Id(i,j);
      int ID_backup=ID;
      ptrs[ID_backup]=std::make_shared<Node>();
      while(ID!=-1 && distance[ID]!=0){
        if(letter[ID]==0){
          if(cur->left==NULL){
              cur->left=std::make_shared<Node>();
              cur->left->parent=cur;
          }
          cur=cur->left;
        }
        else if(letter[ID]==1){
          if(cur->right==NULL){
              cur->right=std::make_shared<Node>();
              cur->right->parent=cur;
          }
          cur=cur->right;
        }
        ID=next[ID];
        if(ID==-1 || distance[ID]==0){
          cur->id=ID_backup;
          ptrs[ID_backup]=cur;
        }
      }
    }
  }

    int rows = 1;
    int cols = N+1;
    int** matrix = new int*[N];
    for (int i = 0; i < N; ++i){
      matrix[i] = new int[cols];
      matrix[i][0]=0;
    }

    for (int j = 1; j < cols; ++j)
      matrix[0][j]=j-1;

    matrix[0][0]=N;
    PNode* last=NULL;
    start=std::chrono::steady_clock::now();
    while(1){
      applyseq_greedy_max(ptrs,root,last,path,matrix,rows,a,distance,letter,next,outputs);
      if(simplify(matrix,rows)==true) break;      
    }
    end=std::chrono::steady_clock::now();

    /*
  PNode* last = NULL;

  int is_homing = 0;
  int np = (N*(N-1))/2;
  int posit = 0;
  int home[np];
  for(int i=0; i<N; i++){
    for(int j=0; j<i; j++)
      home[posit++] = Id(i,j);
  }

  int np_pos = 0;
  int pid = 0;
  while (is_homing==0) {
    posit=0;
    int min_dist=N*N;
    for(int i=0;i<N;i++){
      for(int j=0;j<i;j++){
        if(home[posit]!=-1 && distance[home[posit]]>0 && distance[home[posit]]<min_dist){
          min_dist=distance[home[posit]];
          pid = home[posit];
        }
        ++posit;
      }
    }
    while(pid!=-1 && distance[pid]>0){ // loop until current pair merged or seperated
      posit = 0;
      int let = letter[pid];
      insertToPath(let, path, last);
      for(int i=0; i<N; i++){
        for(int j=0; j<i; j++){
          if(home[posit]!=-1){
            if(let==letter[home[posit]]){
              home[posit]=next[home[posit]];
              if(home[posit]!=-1){
              int s_1 = s1fromId(home[posit]);
              int s_2 = s2fromId(home[posit],s_1);
              if(s_1==s_2) home[posit]=-1;}
            }
            else{
              int s_1 = s1fromId(home[posit]);
              int s_2 = s2fromId(home[posit],s_1);
              if(outputs[let][s_1]!=outputs[let][s_2])
                home[posit] = -1;  
              else{
                if(a[let][s_1]==a[let][s_2])
                  home[posit] = -1;
                else
                  home[posit] = Id(a[let][s_1], a[let][s_2]);
              }
            }
          }
          ++posit;
        }
      }
      pid = next[pid];
    }

    posit=0;
    np_pos=0;
    for(int i=0;i<N;i++){
        for(int j=0;j<i;j++){
          if(home[posit]!=-1)
            ++np_pos;
          ++posit;
        }
      }

    is_homing = 1;
    if(np_pos!=0) is_homing = 0;

    
    PNode* pnode=path;
    PNode* backup=pnode;
    ord = 0;
    while (pnode) {
      cout << (char)(pnode->letter+97) << " "<<std::flush;
    pnode = pnode->next;
    ++ord;
    }
    
    int* sh_path= new int [ord];
    for(int j=0; j<ord; ++j){
      sh_path[j] = backup->letter;
      backup = backup->next;
    }
    cout<< "\n \n"<<isHomingSequence(a,outputs,N,O,sh_path,ord)<< "\n"<< std::flush;
    
    //}
  
  */

  delete distance;
  delete next;
  delete letter;
  delete que;

}

////////////////////////////////// GREEDY SS /////////////////////////////////////////////////////////////////

void greedyHeuristic_naive(int* a, int* iap, int* ia, int N, int P, PNode* &path, int state, int n) {
  int noOfPair = (N * (N + 1)) / 2;
  int* actives = new int[n];
  int* active_marker = new int[N];

  int* distance = new int[noOfPair];
  int* next = new int[noOfPair];
  int* letter = new int[noOfPair];
  int* que = new int[noOfPair];

  for (int i = 0; i < noOfPair; i++) {
    distance[i] =  -1;
  }

  //BFS queue for the pairs
  int qs = 0;
  int qe = 0;

  for (int i = 0; i < N; ++i) {
    int id = Id(i, i);
    distance[id] = 0;
    que[qe++] = id;
  }

  //there are more nodes in the queue
  while (qs < qe && qe < noOfPair) {
    int q_id = que[qs++];
    int q_dist = distance[q_id];

    //will process the pair with id q_id now
    int q_s1 = s1fromId(q_id); //the first state in the pair
    int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)

    for (int p = 0; p < P; p++) {
      int* p_ia = &ia[p * N]; //this is the inverse automata for letter p
      int* p_iap = &iap[p * (N + 1)]; //and its state pointers
      int iap_s1_limit = p_iap[q_s1 + 1];
      int iap_s2_limit = p_iap[q_s2 + 1];

      if (p_iap[q_s2] == iap_s2_limit) continue;

      for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < iap_s1_limit; ++iap_s1_ptr) {
        int ia_s1 = p_ia[iap_s1_ptr];
        for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < iap_s2_limit; ++iap_s2_ptr) {
          int ia_s2 = p_ia[iap_s2_ptr];
          int ia_id = Id(ia_s1, ia_s2);

          if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
            distance[ia_id] = q_dist + 1;

            next[ia_id] = q_id;
            letter[ia_id] = p;
            que[qe++] = ia_id;
          }
        }
      }
    }
  }

  int* levels = new int[200];
  memset(levels, 0, 200 * sizeof(int));

  for (int i = 0; i < N; ++i) {
    for (int j = 0; j <= i; ++j) {
      int id = Id(i, j);

      if (distance[id] == -1) {
        for (int p = 0; p < P; p++) {
          int ts1 = a[p * N + i];
          int ts2 = a[p * N + j];

          int tid = Id(ts1, ts2);
          //cout << "tid " <<  tid << ": distance is " << distance[tid] << endl;
        }

        //cout << "automaton is not synchronizing. pair " << id << " - (" << i << ", " << j << ") is not mergable\n";
        exit(1);
      } else {
        levels[distance[id]]++;
      }
    }
  }

#ifdef DEBUG
  int lvl = 0;
  while (levels[lvl] > 0) {
    cout << "lvl " << lvl++ << ": " << levels[lvl] << endl;
  }
  cout << endl;
#endif

  PNode* last = NULL;
  memset(active_marker, 0, sizeof(int) * N);

  int no_actives = n;
  int c = 0;
  for (int i = 0; i < n; i++) {
    if(i != state)
    {
      if(i > state)
      {
        actives[c] = getUniquePairID(state, i, n);
      }
      else
      {
        actives[c] = getUniquePairID(i, state, n);
      }
      //int j = fromUniquePairID(actives[c], n)[0];
      //int k = fromUniquePairID(actives[c], n)[1];
      //cout << j << " " << k << endl;   
      c++;
    } 
  }
  actives[c] = N-1;
  //int j = fromUniquePairID(actives[c], n)[0];
  //int k = fromUniquePairID(actives[c], n)[1];
  //cout << j << " " << k << endl;
  int step = 1;
while (no_actives > 1) {
    //cout << "no active states is " << no_actives << endl;
    //find the pair id with minimum distance
    int min_distance = N * N;
    int min_id;
    for (int i = 0; i < no_actives; i++) {
      for (int j = 0; j < i; j++) {
        int s1 = actives[i];
        int s2 = actives[j];

        int id = Id(s1, s2);

        if (min_distance > distance[id]) {
          min_distance = distance[id];
          min_id = id;
          if (min_distance == 1) break;
        }
      }
      if (min_distance == 1) break;
    }

    //apply the path and store it
    int pid = min_id;
    while (distance[pid] > 0) {
      int let = letter[pid];
      insertToPath(let, path, last);

      for (int i = 0; i < no_actives; i++) {
        actives[i] = a[let * N + actives[i]];
      }
      pid = next[pid];
    }

    //reduce the number of active states
    int active_count = 0;
    for (int i = 0; i < no_actives; i++) {
      int act = actives[i];
      if (active_marker[act] != step) {
        actives[active_count++] = act;
        active_marker[act] = step;
      }
    }
    no_actives = active_count;
    step++;
  }

  delete distance;
  delete next;
  delete letter;
  delete que;
  delete actives;
  delete active_marker;
}


/////////////////////////////////////// SynchroP SS /////////////////////////////////////////////////

void synchronizing_check(int *a, int N, int P, int *distance) {
  int* levels = new int[200];
  memset(levels, 0, 200 * sizeof(int));

  for (int i = 0; i < N; ++i) {
    for (int j = 0; j <= i; ++j) {
      int id;
        id = Id(i, j);

      if (distance[id] == -1) {
        for (int p = 0; p < P; p++) {
          int ts1 = a[p * N + i];
          int ts2 = a[p * N + j];
          int tid;
            tid = Id(ts1, ts2);
          //cout << "tid " << tid << ": distance is " << distance[tid] << endl;
        }

        //cout << "automata is not synchronizing. pair " << id << " - (" << i << ", " << j << ") is not mergable\n";
        //out << endl;
        exit(0);
        return;
      }
      else {
        levels[distance[id]]++;
      }
    }
  }

#ifdef DEBUG
  int lvl = 0;
  while (levels[lvl] > 0) {
    cout << "lvl " << lvl++ << ": " << levels[lvl] << endl;
  }
  cout << endl;
#endif

  free(levels);
}

long long costPhi(int *a, int *distance, int *letter, int *actives, int *active_marker, int N, int P, int id, int no_actives, int &step)
{
  while (distance[id] > 0) {
    int let = letter[id];

    for (int i = 0; i < no_actives; i++) {
      actives[i] = a[let*N + actives[i] ];
    }

    int s1, s2;

      s1 = s1fromId(id);
      s2 = s2fromId(id, s1);
      id = Id(a[let*N + s1], a[let*N + s2]);
  }

  //reduce the number of active states
  int active_count = 0;
  for (int i = 0; i < no_actives; i++) {
    int act = actives[i];
    if (active_marker[act] != step) {
      actives[active_count++] = act;
      active_marker[act] = step;
    }
  }
  no_actives = active_count;
  step++;

  long long finalTotalDist = 0;
  for (int i = 0; i < no_actives; i++) {
    for (int j = 0; j < i; j++) {
      int s1 = actives[i];
      int s2 = actives[j];

      int tid;
        tid = Id(s1, s2);

      finalTotalDist += distance[tid];
    }
  }

  return finalTotalDist;

}

void greedyHeuristic_finding(int *a, int *distance, int *letter, int *actives, int * active_marker, int N, int P, PNode* &path, int state, int n) {

  PNode* last = NULL;
  memset(active_marker, 0, sizeof(int) * N);

  int no_actives = n;
  int c = 0;
  for (int i = 0; i < n; i++) {
    if(i != state)
    {
      if(i > state)
      {
        actives[c] = getUniquePairID(state, i, n);
      }
      else
      {
        actives[c] = getUniquePairID(i, state, n);
      }
      //int j = fromUniquePairID(actives[c], n)[0];
      //int k = fromUniquePairID(actives[c], n)[1];
      //cout << j << " " << k << endl;   
      c++;
    } 
  }
  actives[c] = N-1;
  //int j = fromUniquePairID(actives[c], n)[0];
  //int k = fromUniquePairID(actives[c], n)[1];
  //cout << j << " " << k << endl;

  int* cp_actives = new int[n];
  int step = 1;
  while (no_actives > 1) {
    //cout << "no active states is " << no_actives << endl;
    //find the pair id with minimum phi-cost value 
    unsigned long long int min_cost = LLONG_MAX;
    int min_id;
    for (int i = 0; i < no_actives; i++) {
      for (int j = 0; j < i; j++) {
        int s1 = actives[i];
        int s2 = actives[j];

        int id;
          id = Id(s1, s2);

        memcpy((void*)cp_actives, (void*)actives, sizeof(int) * n);
        unsigned long long int cost = costPhi(a, distance, letter, cp_actives, active_marker, N, P, id, no_actives, step);
        if (min_cost > cost) {
          min_cost = cost;
          min_id = id;
        }
      }
    }
    //cout << min_id << " " << min_cost << " ";
    // cout << "merging pair from level " << min_distance << endl;

    //apply the path and store it
    int pid = min_id;
    int added = 0;
    while (distance[pid] > 0) {
      int let = letter[pid];
      insertToPath(let, path, last);
      added++;

      for (int i = 0; i < no_actives; i++) {
        actives[i] = a[let*N + actives[i] ];
      }

      int s1, s2;

        s1 = s1fromId(pid);
        s2 = s2fromId(pid, s1);
        pid = Id(a[let*N + s1 ], a[let*N + s2 ]);
      
    }

    //cout << added << endl;

    //reduce the number of active states
    int active_count = 0;
    for (int i = 0; i < no_actives; i++) {
      int act = actives[i];
      if (active_marker[act] != step) {
        actives[active_count++] = act;
        active_marker[act] = step;
      }
    }
    no_actives = active_count;
    step++;
  }


  free(cp_actives);
}


//a is automata a[i][j] -> state j goes to a[i][j] with letter j
//iap is inverse automata pointers -> ia[i][iap[i][j] ... ia[i][j+1]] keeps the state ids which go to state j with letter i
//there are N states and p letters in the automata
void SynchroP(int* a, int* iap, int* ia, int N, int P, PNode* &path, int state, int n) {

  int noOfPair = (N * (N + 1)) / 2;
  int* actives = new int[n];

  int* distance = new int[noOfPair];
  int* letter = new int[noOfPair];
  int* que = new int[noOfPair];

  int* active_marker = new int[N];

  for (int i = 0; i < noOfPair; i++) {
    distance[i] = -1;
  }

  //BFS queue for the pairs
  int qs = 0;
  int qe = 0;

  for (int i = 0; i < N; ++i) {
    int id = Id(i, i);
    distance[id] = 0;
    que[qe++] = id;
  }

  //there are more nodes in the queue
  while (qs < qe) {
    int q_id = que[qs++];
    int q_dist = distance[q_id];

    //will process the pair with id q_id now
    int q_s1 = s1fromId(q_id); //the first state in the pair
    int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)

#ifdef DEBUG
    cout << "will process " << q_s1 << " " << q_s2 << " with id  " << q_id << " with distance " << q_dist << endl;
#endif

    int* p_ia = ia; //this is the inverse automata for letter p
    int* p_iap = iap; //and its state pointers

    for (int p = 0; p < P; p++) {

      for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < p_iap[q_s1 + 1]; ++iap_s1_ptr) {
        int ia_s1 = p_ia[iap_s1_ptr];
        for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < p_iap[q_s2 + 1]; ++iap_s2_ptr) {
          int ia_s2 = p_ia[iap_s2_ptr];
          int ia_id = Id(ia_s1, ia_s2);
          if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
            distance[ia_id] = q_dist + 1;
            letter[ia_id] = p;
            que[qe++] = ia_id;
          }
        }
      }
      p_ia += N; //this is the inverse automata for letter p
      p_iap += (N + 1); //and its state pointers
    }
  }

  synchronizing_check(a, N, P, distance);

  greedyHeuristic_finding(a, distance, letter, actives, active_marker, N, P, path, state, n);

  delete distance;
  delete letter;
  delete que;
  delete actives;
  delete active_marker;

}


///////////////////////////////////////// SynchroP HS ///////////////////////////////////

void SynchroP_HS(unsigned short ** a, unsigned short** outputs, int* iap, int* ia, int N, int P,int O, PNode* &path,Time &start,Time &end) {

  start=std::chrono::steady_clock::now(); // correct it!

  int noOfPair = (N * (N + 1)) / 2;
  int* distance = new int[noOfPair];
  int* next = new int[noOfPair];
  int* letter = new int[noOfPair];
  int* que = new int[noOfPair];
  int ord=0;
  for (int i = 0; i < noOfPair; i++) {
    distance[i] =  -1;
  }

  //BFS queue for the pairs
  int qs = 0;
  int qe = 0;

  for (int i = 0; i < N; ++i) {
    int id = Id(i, i);
    distance[id] = 0;
    que[qe++] = id;
  }

  bool output_check = true;
  //there are more nodes in the queue
  while (qs < qe && qe < noOfPair) {
    int q_id = que[qs++];
    int q_dist = distance[q_id];

    //will process the pair with id q_id now
    int q_s1 = s1fromId(q_id); //the first state in the pair
    int q_s2 = s2fromId(q_id, q_s1); //the second state in the pair (we are sure that q_s1 >= q_s2)
    
    int ia_id;
    if(output_check){
      for(int i=0; i<N; i++){
        for(int j=0; j<i; j++){
          for(int p=0; p<P; p++){
            if(outputs[p][i] != outputs[p][j]){
              ia_id = Id(i,j);
              if(distance[ia_id] < 0){
                distance[ia_id] = 1;
                next[ia_id] = -1; // -1 stands for the state q*
                letter[ia_id] = p;
                que[qe++] = ia_id;
              }
            }
          }
        }
      }
      output_check = false;
    }

    for (int p = 0; p < P; p++) {
      int* p_ia = &ia[p * N]; //this is the inverse automata for letter p
      int* p_iap = &iap[p * (N + 1)]; //and its state pointers
      int iap_s1_limit = p_iap[q_s1 + 1];
      int iap_s2_limit = p_iap[q_s2 + 1];

      if (p_iap[q_s2] == iap_s2_limit) continue;

      for (int iap_s1_ptr = p_iap[q_s1]; iap_s1_ptr < iap_s1_limit; ++iap_s1_ptr) {
        int ia_s1 = p_ia[iap_s1_ptr];
        for (int iap_s2_ptr = p_iap[q_s2]; iap_s2_ptr < iap_s2_limit; ++iap_s2_ptr) {
          int ia_s2 = p_ia[iap_s2_ptr];
          ia_id = Id(ia_s1, ia_s2);

          if (distance[ia_id] < 0) { //we found an unvisited pair. so we need to add this to the queue
            distance[ia_id] = q_dist + 1;
            next[ia_id] = q_id;
            letter[ia_id] = p;
            que[qe++] = ia_id;
          }
        }
      }
    }
  }

  int* levels = new int[200];
  memset(levels, 0, 200 * sizeof(int));

  for (int i = 0; i < N; ++i) {
    for (int j = 0; j <= i; ++j) {
      int id = Id(i, j);

      if (distance[id] == -1) {
        for (int p = 0; p < P; p++) {
          unsigned short ts1 = a[p][i];
          unsigned short ts2 = a[p][j];
          int tid = Id(ts1, ts2);
          //cout << "tid " <<  tid << ": distance is " << distance[tid] << endl;
        }

        //cout << "automaton is not synchronizing. pair " << id << " - (" << i << ", " << j << ") is not mergable\n";
        exit(1);
      } else {
        levels[distance[id]]++;
      }
    }
  }

  int rows = 1;
    int cols = N+1;
    int** matrix = new int*[N];
    for (int i = 0; i < N; ++i){
      matrix[i] = new int[cols];
      matrix[i][0]=0;
    }

    for (int j = 1; j < cols; ++j)
      matrix[0][j]=j-1;

    matrix[0][0]=N;
    PNode* last=NULL;
    //start=std::chrono::steady_clock::now();
    while(1){
      applyseq_SynchroP(last,path,matrix,rows,a,distance,letter,next,outputs);
      if(simplify(matrix,rows)==true) break;      
    }
    end=std::chrono::steady_clock::now();

/*

  PNode* last = NULL;

  int is_homing = 0;
  int np = (N*(N-1))/2;
  int posit = 0;
  int home[np];
  for(int i=0; i<N; i++){
    for(int j=0; j<i; j++)
      home[posit++] = Id(i,j);
  }

  int np_pos = 0;
  int pid = 0;
  int cost;
  while (is_homing==0) {
    posit=0;
    unsigned long long int min_cost = LLONG_MAX;
    for(int i=0; i<N; i++){
      for(int j=0; j<i; j++){
        if(home[posit]!=-1 && distance[home[posit]]>0){
          cost=0;
          int home_backup[np];
          memmove(home_backup, home, sizeof(home));
          while(home_backup[posit]!=-1 && distance[home_backup[posit]]>0){
            int let = letter[home_backup[posit]];
            int position = 0;
            for(int i=0; i<N; i++){
              for(int j=0; j<i; j++){
                if(home_backup[position]!=-1 && distance[home_backup[position]]>0){
                  int s_1 = s1fromId(home_backup[position]);
                  int s_2 = s2fromId(home_backup[position],s_1);
                  if(outputs[let][s_1] != outputs[let][s_2])
                    home_backup[position] = -1;
                  else{
                    if(a[let][s_1] == a[let][s_2])
                      home_backup[position] = -1;
                    else
                      home_backup[position] = Id(a[let][s_1],a[let][s_2]);
                  }
                }
                ++position;
              }
            }
          }
          
          int position=0;
          for(int i=0; i<N; i++){
            for(int j=0; j<i; j++){
              if(home_backup[position]!=-1 && distance[home_backup[position]]>0)
                cost += distance[home_backup[position]];
              ++position;
            }
          }

          if(cost<min_cost)
            min_cost=cost;
          pid = home[posit];
        }
        ++posit;
      }
    }
    while(pid!=-1 && distance[pid]>0){ // loop until current pair merged or seperated
      posit = 0;
      int let = letter[pid];
      insertToPath(let, path, last);
      for(int i=0; i<N; i++){
        for(int j=0; j<i; j++){
          if(home[posit]!=-1){
            if(let==letter[home[posit]]){
              home[posit]=next[home[posit]];
              if(home[posit]!=-1){
              int s_1 = s1fromId(home[posit]);
              int s_2 = s2fromId(home[posit],s_1);
              if(s_1==s_2) home[posit]=-1;}
            }
            else{
              int s_1 = s1fromId(home[posit]);
              int s_2 = s2fromId(home[posit],s_1);
              if(outputs[let][s_1]!=outputs[let][s_2])
                home[posit] = -1;  
              else{
                if(a[let][s_1]==a[let][s_2])
                  home[posit] = -1;
                else
                  home[posit] = Id(a[let][s_1], a[let][s_2]);
              }
            }
          }
          ++posit;
        }
      }
      pid = next[pid];
    }

    posit=0;
    np_pos=0;
    for(int i=0;i<N;i++){
        for(int j=0;j<i;j++){
          if(home[posit]!=-1)
            ++np_pos;
          ++posit;
        }
      }

    is_homing = 1;
    if(np_pos!=0) is_homing = 0;
    }

        PNode* pnode=path;
    PNode* backup=pnode;
    ord = 0;
    while (pnode) {
      cout << (char)(pnode->letter+97) << " "<<std::flush;
    pnode = pnode->next;
    ++ord;
    }
    
    int* sh_path= new int [ord];
    for(int j=0; j<ord; ++j){
      sh_path[j] = backup->letter;
      backup = backup->next;
    }
    cout<< "\n \n"<<isHomingSequence(a,outputs,N,O,sh_path,ord)<< "\n"<< std::flush;
    */
  delete distance;
  delete next;
  delete letter;
  delete que;

}


///////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char**argv)
{

  if(argc < 6)
  {
    cout << "Usage: " << argv[0] << " no_states input_size output_size state_of_UIO rand_seed\n" << endl ;

    cout << "Usage: " << argv[0] << " no_states input_size output_size \"all\" rand_seed\n" << endl;
    return 0;
  }

  //Setup for the values from arguments
  unsigned short N = atoi(argv[1]);
  unsigned short P = atoi(argv[2]);
  unsigned short O = atoi(argv[3]);
  int state;

  if(strncmp(argv[4],"all",3) == 0)
  {
    state = -1;
  }
  else
  {
    state = atoi(argv[4]);
  }
  uint seed = atoi(argv[5]);
  uint sd=seed;

  if(state >= N)
  {
    cout << "Selected state can't be greater than number of states." << endl;
    return 0;
  }

  //Creating 2 matrices for next states and outputs acording to inputs
  unsigned short ** outputs = new unsigned short*[P];
  unsigned short ** nextStates = new unsigned short*[P];
  //Randomly filling these matrices created above
  for (int i = 0; i < P; i++)
  {
    nextStates[i] = new unsigned short[N];
    outputs[i] = new unsigned short[N];
    for(int j = 0; j < N; j++)
    {
      nextStates[i][j] = ((unsigned short)rand_r(&seed)) % N;
      outputs[i][j] = ((unsigned short)rand_r(&seed)) % O;
    }
  }


unsigned short * row = new unsigned short[N];
	for(int n1 = 0 ;n1<N ; n1++)
	{
		row[n1] = 0;
	}

	for (int p=0; p<P; p++)
	{
		for(int n1 = 0 ;n1<N ; n1++)
		{
			row[nextStates[p][n1]] = 1;
		}
		for(int n1 = 0 ;n1<N ; n1++)
		{
			for(int n2=n1+1; n2<N; n2++)
			{
				if(nextStates[p][n1]==nextStates[p][n2] && outputs[p][n1]==outputs[p][n2] )
				{
					while(true)
					{
            int randomIndex = rand() % N;
						if(row[randomIndex] == 0)
						{
              //cout << p << " " << n1 << " " << n2 << " " << randomIndex << endl;
							nextStates[p][n2] = randomIndex;
							row[randomIndex]=1;
              break;
						}
					}
				}
			}
		}
		for(int n1 = 0 ;n1<N ; n1++)
		{
			row[n1] = 0;
		}
	}

  int noOfPair = N*(N-1)/2;
  Time pair_start = std::chrono::steady_clock::now();
  unsigned short ** pairAutomata = convertToPairAutomata(N, P, noOfPair, nextStates, outputs);
  Time pair_end = std::chrono::steady_clock::now();

#ifdef PRINTAUTOMATA
  printFSM(nextStates, outputs, N, P);
  cout << endl;
#endif

#ifdef PRINTPAIR
  printPairAutomata(pairAutomata, N, P);
  cout << endl;
#endif

  int* automata = new int[(noOfPair+1)*P];

  int pos=0;
  for(int j=0;j<P;++j){
    for(int k=0;k<noOfPair+1;++k){
      automata[pos++]=pairAutomata[j][k];
    }
  }
  /*
  int* automaton = new int[N*P];
  int pos=0; //correct it !
  for(int j=0;j<P;++j){
    for(int k=0;k<N;++k){
      automaton[pos++]=nextStates[j][k];
    }
  }*/
  /*
PNode* pathh = NULL;
  int* inv_automata_ptrsh = new int[P * (N+1)];
  int* inv_automatah = new int[P * N];
  
  for (int i = 0; i < P; ++i) {
    int* ah = &(automaton[i * N]);
    int* iah = &(inv_automatah[i * N]);
    int* iaph = &(inv_automata_ptrsh[i * (N + 1)]);

    memset(iaph, 0, sizeof(int) * (N + 1));
    for (int j = 0; j < N; j++) {iaph[ah[j] + 1]++;}
    for (int j = 1; j <= N; j++) {iaph[j] += iaph[j - 1];}
    for (int j = 0; j < N; j++) {iah[iaph[ah[j]]++] = j;}
    for (int j = N; j > 0; j--) {iaph[j] = iaph[j - 1];} iaph[0] = 0;
  }


Time fast_homing_start1,fast_homing_end1,fast_homing_start2,fast_homing_end2;
fastHomingHeuristic(nextStates, outputs, inv_automata_ptrsh, inv_automatah, N, P, O, pathh,fast_homing_start1,fast_homing_end1,fast_homing_start2,fast_homing_end2);

  int* automatong= new int[N*P];
  pos=0;
  for(int j=0;j<P;++j){
    for(int k=0;k<N;++k){
      automatong[pos++]=nextStates[j][k];
    }
  }
  
PNode* pathg = NULL;
  int* inv_automata_ptrsg = new int[P * (N+1)];
  int* inv_automatag = new int[P * N];
  
  for (int i = 0; i < P; ++i) {
    int* ag = &(automatong[i * N]);
    int* iag = &(inv_automatag[i * N]);
    int* iapg = &(inv_automata_ptrsg[i * (N + 1)]);

    memset(iapg, 0, sizeof(int) * (N + 1));
    for (int j = 0; j < N; j++) {iapg[ag[j] + 1]++;}
    for (int j = 1; j <= N; j++) {iapg[j] += iapg[j - 1];}
    for (int j = 0; j < N; j++) {iag[iapg[ag[j]]++] = j;}
    for (int j = N; j > 0; j--) {iapg[j] = iapg[j - 1];} iapg[0] = 0;
  }
Time greedy_homing_start1,greedy_homing_end1,greedy_homing_start2,greedy_homing_end2;
greedyHomingHeuristic(nextStates, outputs, inv_automata_ptrsg, inv_automatag, N, P, O, pathg,greedy_homing_start1,greedy_homing_end1,greedy_homing_start2,greedy_homing_end2);

PNode* ege = NULL;
Time greedy_homing_max_start,greedy_homing_max_end;
//greedyHomingHeuristic_max(nextStates, outputs, inv_automata_ptrsg, inv_automatag, N, P, O, ege,greedy_homing_max_start,greedy_homing_max_end);

PNode* pathsh = NULL;
Time SynchroP_hs_start,SynchroP_hs_end;
//SynchroP_HS(nextStates, outputs, inv_automata_ptrsg, inv_automatag, N, P, O, pathsh,SynchroP_hs_start,SynchroP_hs_end);

*/
////////////////////////////////// HOMING AUTOMATON HEURISTICS //////////////////////////////////////// 

  
  unsigned short N_=noOfPair+1;
  int* inv_automata_ptrs = new int[P * (N_+1)];
  int* inv_automata = new int[P * N_];

  for (int i = 0; i < P; ++i) {
    int* a = &(automata[i * N_]);
    int* ia = &(inv_automata[i * N_]);
    int* iap = &(inv_automata_ptrs[i * (N_ + 1)]);

    memset(iap, 0, sizeof(int) * (N_ + 1));
    for (int j = 0; j < N_; j++) {iap[a[j] + 1]++;}
    for (int j = 1; j <= N_; j++) {iap[j] += iap[j - 1];}
    for (int j = 0; j < N_; j++) {ia[iap[a[j]]++] = j;}
    for (int j = N_; j > 0; j--) {iap[j] = iap[j - 1];} iap[0] = 0;
  }

if(state == -1)
{
  for(int i = 0; i < N; i++)
  {
  	PNode* path = NULL;
    cout << "UIO SEQUENCE FOR STATE " << i << endl;
    cout << "-----------------------" << endl;
    Time greedy_start=std::chrono::steady_clock::now();
    greedyHeuristic_naive(automata, inv_automata_ptrs, inv_automata, N_, P, path, i, N);
    Time greedy_end=std::chrono::steady_clock::now();
    int greedy_time = std::chrono::duration_cast<std::chrono::microseconds>(greedy_end - greedy_start).count();

    PNode* pnode = path;
    int plength = 0;
    
    while (pnode) {
      #ifdef PRINTPATH
      if(plength == 0)
      {
        cout << "Greedy path: ";
      }
        cout << (char)(pnode->letter+97) << " ";
      #endif
      pnode = pnode->next;
      plength++;
    }
    cout << endl;
    cout << "Greedy path length: " << plength << endl;
    cout << "Greedy Time: " << greedy_time << " microseconds" << endl << endl;
   
    PNode* pathp = NULL;

    Time SynchroP_start=std::chrono::steady_clock::now();
    SynchroP(automata, inv_automata_ptrs, inv_automata, N_, P, pathp, i, N);
    Time SynchroP_end=std::chrono::steady_clock::now();
    long sp_time = std::chrono::duration_cast<std::chrono::microseconds>(SynchroP_end - SynchroP_start).count();
    pnode = pathp;
    int sp_length = 0;
        
    while (pnode) {
      #ifdef PRINTPATH
      if(sp_length == 0)
      {
        cout << "SynchroP path: ";
      }
      cout << (char)(pnode->letter+97) << " ";
      #endif
      pnode = pnode->next;
      sp_length++;
    }
    cout << endl;
    cout << "SynchroP path length: " << sp_length << endl;
    cout << "SynchroP Time: " << sp_time << " microsseconds" << endl << endl;
  } 
} 
else
{

  PNode* path = NULL;
  cout << "UIO SEQUENCE FOR STATE " << state << endl;
  cout << "-----------------------" << endl;
  Time greedy_start=std::chrono::steady_clock::now();
  greedyHeuristic_naive(automata, inv_automata_ptrs, inv_automata, N_, P, path, state, N);
  Time greedy_end=std::chrono::steady_clock::now();
  int greedy_time = std::chrono::duration_cast<std::chrono::microseconds>(greedy_end - greedy_start).count();

  PNode* pnode = path;
  int plength = 0;
  
  while (pnode) {
    #ifdef PRINTPATH
    if(plength == 0)
    {
      cout << "Greedy path: ";
    }
      cout << (char)(pnode->letter+97) << " ";
    #endif
    pnode = pnode->next;
    plength++;
  }
  cout << endl;
  cout << "Greedy path length: " << plength << endl;
  cout << "Greedy Time: " << greedy_time << " microseconds" << endl << endl;
 
  PNode* pathp = NULL;

  Time SynchroP_start=std::chrono::steady_clock::now();
  SynchroP(automata, inv_automata_ptrs, inv_automata, N_, P, pathp, state, N);
  Time SynchroP_end=std::chrono::steady_clock::now();
  long sp_time = std::chrono::duration_cast<std::chrono::microseconds>(SynchroP_end - SynchroP_start).count();
  pnode = pathp;
  int sp_length = 0;
      
  while (pnode) {
    #ifdef PRINTPATH
    if(sp_length == 0)
    {
      cout << "SynchroP path: ";
    }
    cout << (char)(pnode->letter+97) << " ";
    #endif
    pnode = pnode->next;
    sp_length++;
  }
  cout << endl;
  cout << "SynchroP path length: " << sp_length << endl;
  cout << "SynchroP Time: " << sp_time << " microsseconds" << endl; 
}
  
  
  
/*
  PNode* pnode = pathh; //correct it! 
  int fast_length = 0;
  while (pnode) {
    #ifdef PRINTPATH
      cout << (char)(pnode->letter+97) << " ";
    #endif
    pnode = pnode->next;
    fast_length++;
  }


  pnode = pathg; // correct it!
  int home_gr_length = 0;
  while (pnode) {
    #ifdef PRINTPATH
      cout << (char)(pnode->letter+97) << " ";
    #endif
    pnode = pnode->next;
    home_gr_length++;
  }

  pnode = ege; // correct it!
  int home_gr_max_length = 0;
  while (pnode) {
    #ifdef PRINTPATH
      cout << (char)(pnode->letter+97) << " ";
    #endif
    pnode = pnode->next;
    home_gr_max_length++;
  }

  pnode = pathsh; // correct it!
  int sph_length = 0;
  while (pnode) {
    #ifdef PRINTPATH
      cout << (char)(pnode->letter+97) << " ";
    #endif
    pnode = pnode->next;
    sph_length++;
  }*/
  
  
  #ifdef CHECKHS
    std::ofstream checkhs;
    checkhs.open("isHS.txt",std::ios_base::out | std::ios_base::app);
    int* sh_path= new int [plength];
    for(int j=0;j<plength;++j){
      sh_path[j]=path->letter;
      path=path->next;
    }
    if(isHomingSequence(nextStates,outputs,N,O,sh_path,plength)=='F')
      checkhs << isHomingSequence(nextStates,outputs,N,O,sh_path,plength);
    checkhs.close();
  #endif

  Time shortest_start=std::chrono::steady_clock::now();
  //int shortest_length=shortestHomingSequence(nextStates,outputs,N,P,O);
  Time shortest_end=std::chrono::steady_clock::now();
  int shortestHS_time=std::chrono::duration_cast<std::chrono::microseconds>(shortest_end - shortest_start).count();

  for(int i = 0; i < P; i++)
  {
    delete outputs[i];
    delete nextStates[i]; 
  }

  delete outputs;
  delete nextStates;
  //delete automata;

  
  //int pair_time = std::chrono::duration_cast<std::chrono::microseconds>(pair_end - pair_start).count();
  //int fast_homing_time1 = std::chrono::duration_cast<std::chrono::microseconds>(fast_homing_end1 - fast_homing_start1).count();
  //int fast_homing_time2 = std::chrono::duration_cast<std::chrono::microseconds>(fast_homing_end2 - fast_homing_start2).count();
  //int home_gr_time1 = std::chrono::duration_cast<std::chrono::microseconds>(greedy_homing_end1 - greedy_homing_start1).count();
  //int home_gr_time2 = std::chrono::duration_cast<std::chrono::microseconds>(greedy_homing_end2 - greedy_homing_start2).count();
  //int sph_time = std::chrono::duration_cast<std::chrono::microseconds>(SynchroP_hs_end - SynchroP_hs_start).count();
  //int home_gr_max_time = std::chrono::duration_cast<std::chrono::microseconds>(greedy_homing_max_end - greedy_homing_max_start).count();

  //cout<<N<<","<<P<<","<<O<<","<<sd<<","<<shortest_length<<","<<shortestHS_time<<",";
  //cout<<N<<","<<P<<","<<O<<","<<sd<<endl;
  //cout<<home_gr_length<<","<<home_gr_time1<<","<<home_gr_time2<<endl;//<<","<<home_gr_max_length<<","<<home_gr_max_time;
  //cout<<fast_length<<","<<fast_homing_time1<<","<<fast_homing_time2<<endl;
  //cout<<","<<sph_length<<","<<sph_time<<","<<plength<<","<<pair_time<<","<<greedy_time<<","<<sp_length<<","<<sp_time;

  return 0;
}